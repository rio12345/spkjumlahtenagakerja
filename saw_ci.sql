-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 27 Okt 2021 pada 08.08
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `saw_ci`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_admin`
--

CREATE TABLE `tb_admin` (
  `user` varchar(16) DEFAULT NULL,
  `pass` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_admin`
--

INSERT INTO `tb_admin` (`user`, `pass`) VALUES
('admin', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_alternatif`
--

CREATE TABLE `tb_alternatif` (
  `kode_alternatif` varchar(16) NOT NULL,
  `nama_alternatif` varchar(256) DEFAULT NULL,
  `keterangan` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_alternatif`
--

INSERT INTO `tb_alternatif` (`kode_alternatif`, `nama_alternatif`, `keterangan`) VALUES
('A01', '1-10 orang pekerja', 'ketenagakerjaan'),
('A02', '10-20 orang pekerja', 'ketenagakerjaan'),
('A03', '20-25 orang pekerja', 'ketenagakerjaan'),
('A04', '25-30 orang pekerja', 'ketenagakerjaan'),
('A05', '30-35 orang pekerja', 'ketenagakerjaan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kriteria`
--

CREATE TABLE `tb_kriteria` (
  `kode_kriteria` varchar(16) NOT NULL,
  `nama_kriteria` varchar(256) DEFAULT NULL,
  `atribut` varchar(16) DEFAULT NULL,
  `bobot` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_kriteria`
--

INSERT INTO `tb_kriteria` (`kode_kriteria`, `nama_kriteria`, `atribut`, `bobot`) VALUES
('C01', 'Jumlah Anggaran', 'benefit', 5),
('C02', 'Tenaga kerja menurut 45% dari anggaran', 'benefit', 5),
('C03', 'Kerja Cepat selesai dalam waktu 3 bulan', 'benefit', 5),
('C04', 'Kriteria Untuk pembangunan Pelabuhan ', 'benefit', 5),
('C05', 'Kriteria Untuk kontrakan ', 'cost', 5),
('C06', 'Kriteria Jumlah harian ', 'benefit', 5),
('C07', 'Kriteria Keadaan cuaca ', 'benefit', 5),
('C08', 'Kriteria Pembangunan ', 'cost', 5),
('C09', 'Kriteria Alat bangunan ketenagakerjaan ', 'benefit', 5),
('C10', 'Kriteria Kualitas material ', 'benefit', 5),
('C11', 'Kkriteria Jumlah Mandor ', 'cost', 5),
('C12', 'Kriteria Jumlah Tukang Besi ', 'benefit', 5),
('C13', 'Kriteria Jumlah Tukang Semen ', 'benefit', 5),
('C14', 'Kriteria Pekerja Alat Berat ', 'cost', 5),
('C15', 'Kriteria bahan material ', 'benefit', 5),
('C16', 'Ukuran Pembangunan ', 'benefit', 5),
('C17', 'Kriteria Pekerja berat ', 'benefit', 5),
('C18', 'Kriteria Tukang batu', 'benefit', 5),
('C19', 'Kriteria Tukang kayu ', 'cost', 5),
('C20', 'Kriteria Tukang Aspal ', 'cost', 5),
('C21', 'Kriteria tukang cat ', 'cost', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_rel_alternatif`
--

CREATE TABLE `tb_rel_alternatif` (
  `ID` int(11) NOT NULL,
  `kode_alternatif` varchar(16) DEFAULT NULL,
  `kode_kriteria` varchar(16) DEFAULT NULL,
  `nilai` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_rel_alternatif`
--

INSERT INTO `tb_rel_alternatif` (`ID`, `kode_alternatif`, `kode_kriteria`, `nilai`) VALUES
(51, 'A03', 'C04', 3),
(50, 'A02', 'C04', 2),
(34, 'A01', 'C01', 5),
(35, 'A02', 'C01', 5),
(36, 'A03', 'C01', 5),
(37, 'A04', 'C01', 5),
(38, 'A05', 'C01', 5),
(39, 'A01', 'C02', 5),
(40, 'A02', 'C02', 5),
(41, 'A03', 'C02', 4),
(42, 'A04', 'C02', 3),
(43, 'A05', 'C02', 1),
(44, 'A01', 'C03', 1),
(45, 'A02', 'C03', 2),
(46, 'A03', 'C03', 3),
(47, 'A04', 'C03', 4),
(48, 'A05', 'C03', 5),
(49, 'A01', 'C04', 1),
(52, 'A04', 'C04', 4),
(53, 'A05', 'C04', 5),
(54, 'A01', 'C05', 3),
(55, 'A02', 'C05', 4),
(56, 'A03', 'C05', 5),
(57, 'A04', 'C05', 5),
(58, 'A05', 'C05', 5),
(59, 'A01', 'C06', 5),
(60, 'A02', 'C06', 4),
(61, 'A03', 'C06', 4),
(62, 'A04', 'C06', 4),
(63, 'A05', 'C06', 4),
(64, 'A01', 'C07', 4),
(65, 'A02', 'C07', 4),
(66, 'A03', 'C07', 4),
(67, 'A04', 'C07', 4),
(68, 'A05', 'C07', 4),
(69, 'A01', 'C08', 5),
(70, 'A02', 'C08', 5),
(71, 'A03', 'C08', 5),
(72, 'A04', 'C08', 5),
(73, 'A05', 'C08', 5),
(74, 'A01', 'C09', 5),
(75, 'A02', 'C09', 5),
(76, 'A03', 'C09', 5),
(77, 'A04', 'C09', 5),
(78, 'A05', 'C09', 5),
(79, 'A01', 'C10', 1),
(80, 'A02', 'C10', 3),
(81, 'A03', 'C10', 3),
(82, 'A04', 'C10', 3),
(83, 'A05', 'C10', 3),
(84, 'A01', 'C11', 5),
(85, 'A02', 'C11', 5),
(86, 'A03', 'C11', 5),
(87, 'A04', 'C11', 3),
(88, 'A05', 'C11', 1),
(89, 'A01', 'C12', 1),
(90, 'A02', 'C12', 3),
(91, 'A03', 'C12', 5),
(92, 'A04', 'C12', 5),
(93, 'A05', 'C12', 5),
(94, 'A01', 'C13', 1),
(95, 'A02', 'C13', 3),
(96, 'A03', 'C13', 4),
(97, 'A04', 'C13', 4),
(98, 'A05', 'C13', 5),
(99, 'A01', 'C14', 5),
(100, 'A02', 'C14', 4),
(101, 'A03', 'C14', 3),
(102, 'A04', 'C14', 2),
(103, 'A05', 'C14', 1),
(104, 'A01', 'C15', 5),
(105, 'A02', 'C15', 5),
(106, 'A03', 'C15', 5),
(107, 'A04', 'C15', 5),
(108, 'A05', 'C15', 5),
(109, 'A01', 'C16', 1),
(110, 'A02', 'C16', 5),
(111, 'A03', 'C16', 5),
(112, 'A04', 'C16', 5),
(113, 'A05', 'C16', 5),
(114, 'A01', 'C17', 2),
(115, 'A02', 'C17', 3),
(116, 'A03', 'C17', 4),
(117, 'A04', 'C17', 4),
(118, 'A05', 'C17', 5),
(119, 'A01', 'C18', 1),
(120, 'A02', 'C18', 3),
(121, 'A03', 'C18', 4),
(122, 'A04', 'C18', 4),
(123, 'A05', 'C18', 5),
(124, 'A01', 'C19', 5),
(125, 'A02', 'C19', 4),
(126, 'A03', 'C19', 3),
(127, 'A04', 'C19', 2),
(128, 'A05', 'C19', 1),
(129, 'A01', 'C20', 5),
(130, 'A02', 'C20', 4),
(131, 'A03', 'C20', 3),
(132, 'A04', 'C20', 2),
(133, 'A05', 'C20', 1),
(134, 'A01', 'C21', 5),
(135, 'A02', 'C21', 4),
(136, 'A03', 'C21', 3),
(137, 'A04', 'C21', 2),
(138, 'A05', 'C21', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_alternatif`
--
ALTER TABLE `tb_alternatif`
  ADD PRIMARY KEY (`kode_alternatif`);

--
-- Indeks untuk tabel `tb_kriteria`
--
ALTER TABLE `tb_kriteria`
  ADD PRIMARY KEY (`kode_kriteria`);

--
-- Indeks untuk tabel `tb_rel_alternatif`
--
ALTER TABLE `tb_rel_alternatif`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_rel_alternatif`
--
ALTER TABLE `tb_rel_alternatif`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
