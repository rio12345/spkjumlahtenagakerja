<?php
class Hitung extends CI_Controller
{

    protected $alternatif = array();
    protected $kriteria = array();
    protected $matriks = array();
    protected $normal = array();
    protected $hasil = array();
    protected $total = array();
    protected $rank = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model('hitung_model');
        $this->load->model('kriteria_model');
        $this->load->model('alternatif_model');
    }

    public function index()
    {
        $data['title'] = 'Perhitungan';
        $data['kriteria'] = $this->get_kriteria();
        $data['alternatif'] = $this->get_alternatif();
        $data['matriks'] = $this->get_matriks();
        $data['normal'] = $this->get_normal();
        $data['hasil'] = $this->get_hasil();
        $data['total'] = $this->get_total();
        $data['rank'] = $this->get_rank();

        load_view('hitung', $data);
    }

    public function get_kriteria()
    {
        $rows = $this->kriteria_model->tampil();
        foreach ($rows as $row) {
            $this->kriteria[$row->kode_kriteria] = $row;
        }
        return $this->kriteria;
    }

    public function get_alternatif()
    {
        $rows = $this->alternatif_model->tampil();
        foreach ($rows as $row) {
            $this->alternatif[$row->kode_alternatif] = $row;
        }
        return $this->alternatif;
    }

    public function get_matriks()
    {
        $rows = $this->hitung_model->get_relasi();
        foreach ($rows as $row) {
            $this->matriks[$row->kode_alternatif][$row->kode_kriteria] = $row->nilai;
        }
        return $this->matriks;
    }

    function get_normal()
    {
        $mm = array();

        foreach ($this->matriks as $key => $value) {
            $temp = array();
            foreach ($value as $k => $v) {
                $mm[$k][] = $v;
            }
        }

        foreach ($this->matriks as $key => $value) {
            foreach ($value as $k => $v) {
                if ($this->kriteria[$k]->atribut == 'benefit')
                    $this->normal[$key][$k] = $v / max($mm[$k]);
                else
                    $this->normal[$key][$k] = min($mm[$k]) / $v;
            }
        }
        return $this->normal;
    }

    function get_hasil()
    {
        foreach ($this->normal as $key => $val) {
            foreach ($val as $k => $v) {
                $this->hasil[$key][$k] = $v * $this->kriteria[$k]->bobot;
            }
        }
        return $this->hasil;
    }

    function get_total()
    {
        foreach ($this->hasil as $key => $val) {
            $this->total[$key] = 0;
            foreach ($val as $k => $v) {
                $this->total[$key] += $v;
            }
        }
        return $this->total;
    }

    private function get_rank()
    {
        $data = $this->total;
        arsort($data);
        $no = 1;
        foreach ($data as $key => $value) {
            $this->rank[$key] = $no++;
        }
        return $this->rank;
    }

    public function cetak($ID = NULL)
    {
        $data['kriteria'] = $this->get_kriteria();
        $data['alternatif'] = $this->get_alternatif();
        $data['matriks'] = $this->get_matriks();
        $data['normal'] = $this->get_normal();
        $data['hasil'] = $this->get_hasil();
        $data['total'] = $this->get_total();
        $data['rank'] = $this->get_rank();

        load_view_cetak('hitung_cetak', $data);
    }
}
