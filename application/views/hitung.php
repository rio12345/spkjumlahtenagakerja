<div class="panel panel-primary">
    <div class="panel-heading">Hasil Analisa</div>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tr>
                <thead>
                    <th>#</th>
                    <?php foreach ($kriteria as $key => $val) : ?>
                        <th><?= $key ?></th>
                    <?php endforeach ?>
                </thead>
            </tr>
            <?php foreach ($matriks as $key => $val) : ?>
                <tr>
                    <td><?= $key ?></td>
                    <?php foreach ($val as $k => $v) : ?>
                        <td><?= $v ?></td>
                    <?php endforeach ?>
                </tr>
            <?php endforeach ?>
        </table>
    </div>

</div>

<div class="panel panel-primary">
    <div class="panel-heading"><strong>Normalisasi</strong></div>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tr>
                <thead>
                    <th>#</th>
                    <?php foreach ($kriteria as $key => $val) : ?>
                        <th><?= $key ?></th>
                    <?php endforeach ?>
                </thead>
            </tr>
            <?php foreach ($normal as $key => $val) : ?>
                <tr>
                    <td><?= $key ?></td>
                    <?php foreach ($val as $k => $v) : ?>
                        <td><?= round($v, 4) ?></td>
                    <?php endforeach ?>
                </tr>
            <?php endforeach ?>
        </table>
    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">Perangkingan</div>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tr>
                <thead>
                    <th>#</th>
                    <?php foreach ($kriteria as $key => $val) : ?>
                        <th><?= $key ?></th>
                    <?php endforeach ?>
                    <th>Total</th>
                    <th>Rank</th>
                </thead>
            </tr>
            <tr>
                <td>Bobot Kriteria</td>
                <?php foreach ($kriteria as $key => $val) : ?>
                    <td><?= $kriteria[$key]->bobot ?></td>
                <?php endforeach ?>
                <th></th>
                <th></th>
            </tr>
            <?php foreach ($rank as $key => $val) : ?>
                <tr>
                    <td><?= $key ?></td>
                    <?php foreach ($hasil[$key] as $k => $v) : ?>
                        <td><?= round($v, 4) ?></td>
                    <?php endforeach ?>
                    <th><?= round($total[$key], 4) ?></th>
                    <th><?= $rank[$key] ?></th>
                </tr>
            <?php endforeach ?>
        </table>
    </div>
    <div class="panel-body">
        <a class="btn btn-default" href="<?= site_url('hitung/cetak') ?>" target="_blank"><span class="glyphicon glyphicon-print"></span> Cetak</a>
    </div>
</div>